# 6

Es war Wochenende und trotzdem stand Herr Oink von Ploink früh auf.
Er schaltete den Fernseher an und wartete auf die Werbepause.
Schon wieder kein einziger Werbespot mit Herrn Müller.
Seltsam – sonst war er doch fast ständig zu sehen.
Jeden Tag hatte er mindestens ein innovatives Produkt demonstriert und jetzt auf einmal keins mehr?
Herr Oink von Ploink überlegte, ob es wohl einen Zusammenhang zu seinem Tarnnamen gab.
Verdächtigte die Polizei jetzt Herrn Müller nur weil er den gleichen Nachnamen hatte?

Herr Oink von Ploink holte Oma und Herkules zu einem Parkspaziergang ab.
Es war kühl aber sonnig und Herkules liebte es in den Blätterhaufen zu toben.
Oskar erzählte Oma von seiner Vermutung.
Oma schänkte ihm ein aufmunterndes Omawochenendlächeln:

- "Das ist bestimmt nur ein Zufall.
   Vielleicht hat Herr Müller sich zur Ruhe gesetzt.
   Oder die Werbeagentur probiert etwas neues aus."

Oskar glaubte nicht an Zufälle.
Alles, was in dieser Stadt passierte, hatte irgendeinen Grund.
Es war nicht immer offensichtlich, von wem welche Dinge warum gemacht wurden.
Doch wenn man sich ein wenig damit beschäftigte, fand man meistens eine Erklärung.

- "Ich glaube nicht, dass er schon alt genug für den Ruhestand war.
   Und jeder liebte Herrn Müller – warum sollte die Werbeagentur etwas Neues ausprobieren?"
- "Wir können ihn ja anrufen und fragen."

schlug Oma vor.
Früher hätte Oskar das für einen Witz gehalten und angefangen zu lachen.
Inzwischen wusste er, dass Oma solche Ideen ernst meinte.
Bestimmte Dinge, die einfach so waren, stellte sie einfach so infrage.
Beispielsweise rief man nicht einfach bei irgendeinem Fernsehstar an.
Doch als Oskar darüber nachdachte, fiel ihm dafür eigentlich kein Grund ein.

Nachdem einer von ihnen ausgiebig in mehreren Blätterhaufen getobt hatte, machten die drei sich also auf den Weg zur nächsten Telefonzelle.
Sie mussten eine Weile laufen, denn es gab kaum noch welche.
Im Telefonbuch suchten sie nach Martin Müller.
Es gab tatsächlich nur einen einzigen Eintrag, also riefen sie dort an.

Diverse Pieptöne wanderten von Hörmuschel zu Ohrmuschel.
Keiner ging ran.
Oskar legte auf und beschloss, es später noch einmal zu versuchen.
Bestimmt hatte ein Fernsehstar am Wochenende auch Besseres zu tun, als ans Telefon zu gehen.

Die drei schlenderten die Hauptstraße auf der anderen Seite vom Park entlang.
Plötzlich bliebt Oskar abrupt stehen und deutete auf den Bürgersteig in einiger Entfernung.
Er hatte Loch Nummer 3 entdeckt.

- "Das ist doch nicht normal.
   So viele Löcher habe ich noch nie in so kurzer Zeit im Bürgersteig gesehen.
   Keines von ihnen ist abgesperrt.
   Was ist denn bloß los in der Abteilung für Straßenausbesserung?"

Oma schüttelte nachdenklich den Kopf.
Oma wusste immer auf jede Frage eine Antwort und für jedes Problem eine Lösung.
Was die Löcher anging, war sie aber ratlos.

- "Am Montag fahre ich noch einmal mit Herkules zur Abteilung für Straßenausbesserung und frage nach.
   Seit unserem letzten Besuch dort ist ja schon eine Weile vergangen und inzwischen haben sie bestimmt Fortschritte gemacht."

hoffte sie.
So richtig überzeugt wirkte Oma aber nicht und das bemerkte auch Oskar.
Er ließ sich nichts anmerken und schlug vor, sich in ein nahegelegenes Café zu setzen.

Als ARABU, die Allgemeinen Regelung von Arbeitszeiten und Bezahltem Urlaub, für alle die gleichen Arbeitszeiten festlegte, wurden Restaurants explizit ausgeschlossen.
Keiner wusste so genau, warum es diese Ausnahme gab, aber es ermöglichte den Leuten auch außerhalb ihrer Arbeitszeit und sogar am Wochenende in ein Restaurant zu gehen.
Inzwischen hatten sich alle so sehr daran gewöhnt, dass es wahrscheinlich schwierig wäre, die Regelung nachträglich zu ändern.

Am Fenster fanden sie einen freien Tisch mit gepolsterter Bank.
Beide bestellten Mamorkuchen doch die Kellnerin teilte ihnen mit, dass es leider nur noch Zitronenkuchen gab.
Oma kicherte.
Also wurde ein Stück Zitronenkuchen bestellt und ein weiteres nur widerstrebend.
Dazu gab es zweimal heiße Schokolade und einen Napf mit Wasser (für Herkules).

Auf dem Tisch lag eine alte, zerlesene Tageszeitung.
Darauf war ein Bild von Oskars Vorgesetztem und der Abteilungsleiterin zu sehen.
Neugierig schlug er die Zeitung auf und begann mittendrin in dem viel zu langen Artikel zu lesen.

- "[…] konnten jegliche Sicherheitsbedenken in der Firma ausgeschlossen werden.
   Die Polizei geht derzeit davon aus, dass die Telefonanlage außerhalb des Gebäudes manipuliert wurde und so der Anruf umgeleitet werden konnte.
   Sicherheit ist und bleibt ein wichtiges Merkmal in der vertrauensvollen Zusammenarbeit mit den wertgeschätzten Kunden, versicherte die Abteilungsleiterin."

Interessant, dachte sich Oskar.
Das bedeutete entweder, die Ermittlungen gegen ihn wurden eingestellt oder nur der Öffentlichkeit sollte dieses Bild vermittelt werden.
Er zeigte Oma den Artikel, die ihn aufmerksam (und vollständig) las.

- "Wusstest du, dass sich die Quartalszahlen verglichen mit einem früheren Quartal in eurer Abteilung erneut verbessert haben?"

fragte Oma.

Oskar wusste.
Oskar wusste sogar, welches frühere Quartal hier gemeint war.
Jegliche Statistiken dieser Art basierten auf dem Vergleich mit den Zahlen vor Gründung der Firma.
Auf diese Weise konnte man immer behaupten, der Umsatz, die Effizienz, die Kundenanzahl, die Mitarbeiteranzahl und all die anderen wichtigen Kennzahlen hätten sich kontinuierlich verbessert.
Der vergleich mit dem vorherigen Monat oder dem Vorjahr interessierte keinen.
Insbesondere Oskars Vorgesetzter sorgte dafür, dass es jedem Mitarbeiter erschwert wurde, eine solche Statistik zu erzeugen.

Währenddessen saß Sebastian Schmidt, der SEGSI-Einsatzleiter, im Schaukelstuhl auf seinem Balkon und dachte nach.
Am Tag zuvor hatte er noch einige Nachforschungen bei der Bank gemacht, um herauszufinden, wie das Geld vom Konto der Polizei verschwinden konnte.
Es stellte sich heraus, dass er selbst (ohne es zu wissen) von einem der Einsatzwagen in der Bank angerufen hatte und um eine dringende Überweisung gebeten hatte.
Die Sachbearbeiterin für dringende Überweisungen führte den Geldtransfer durch, ohne sich zu vergewissern, mit wem sie gerade sprach.
Das war schließlich die Aufgabe der Abteilung für Identitätsprüfung.
Von offizieller Seite der Bank hieß es also, man sei sich keines Fehlers bewusst.
Alle Bankangestellten hätten sich genau an ihre Aufgaben gehalten.
Eine Rückbuchung sei auch nicht möglich, da die bezahlte Ware ja inzwischen einwandfrei geliefert wurde.
Dafür gab es schließlich drei gefüllte Einsatzwagen als Zeugen.

Sebastian Schmidt seufzte.
Wenn er doch nur herausfinden könnte, wie und wohin die bestellten Absperrungen verschwunden waren.
Dann wäre er den Tätern sicherlich wieder auf der Spur.
Alle Hinweise, denen er bisher nachgegangen war, hatten sich als Sackgassen erwiesen.
