# Glossar

## ARABU

Allgemeinen Regelung von Arbeitszeiten und Bezahltem Urlaub

## HATWAT

Hierarchisch Angeordnete Temporäre und Wiederrufbare Anweisung zur Tätigkeitsunterbrechung

## KABUM

Kritischen Abmahnung für Besonders Unangebrachtes Mitarbeiterverhalten

## SEGSI

Sonderermittlungsgruppe Straßen- und Infrastrukturkriminalität

## VBZSDT

Vorzeitige Beförderung nach Zulässiger Steigerung der Durchschnittlichen Tagesleistung

## VERBOT

Vorzeitigen Entlassung aus dem Regulären Betriebsablauf nach Ordnungsgemäßer Tätigkeit
