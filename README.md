# Herr Oink von Ploink

Diese Geschichte wurde 2018 von [@winniehell] während dem [National Novel Writing Month] geschrieben und ist unter der [Creative Commons Lizenz] (CC BY-SA 4.0) verfügbar.

[@winniehell]: https://winniehell.de
[National Novel Writing Month]: https://nanowrimo.org/participants/winniehell/projects/herr-oink-von-ploink
[Creative Commons Lizenz]: http://creativecommons.org/licenses/by-sa/4.0/
